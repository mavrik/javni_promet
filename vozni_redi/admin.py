from django.contrib import admin
from vozni_redi.models import Regime, Transporter, Station, Route, Schedule, Ride

class RegimeAdmin(admin.ModelAdmin):
    list_display = ( "id", "author", "description")

class TramsporterAdmin(admin.ModelAdmin):
    list_display = ( "id", "name" )

class StationAdmin(admin.ModelAdmin):
    list_display = ( "name", "number", "meaning" )

class RouteAdmin(admin.ModelAdmin):
    list_display = ( "start", "end", "mk_km", "mk_time", "pr_km", "pr_time" )

class ScheduleAdmin(admin.ModelAdmin):
    list_display = ( "line_from", "line_to", "type", "way", "valid_from", "valid_to")

class RideAdmin(admin.ModelAdmin):
    list_display = ( "number", "label", "direction", "time" )

admin.site.register(Regime, RegimeAdmin)
admin.site.register(Transporter, TramsporterAdmin)
admin.site.register(Station, StationAdmin)
admin.site.register(Route, RouteAdmin)
admin.site.register(Schedule, ScheduleAdmin)
admin.site.register(Ride, RideAdmin)