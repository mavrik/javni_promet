from lxml import etree
from django.core.management.base import BaseCommand
from vozni_redi.models import Regime, Transporter, Station, Route, Schedule, Ride

class Command(BaseCommand):

    def handle(self, *args, **options):
        if len(args) < 1:
            print "Missing argument: path to XML file!"
            return

        try:
            f = open(args[0], "rb")
        except IOError:
            print "Cannot open input file."
            return

        tree = etree.parse(f)

        # The structured of fields is dumb - they're not in a hiearchy but just a flat list with
        # pre-set order - hence the assert checking and static dictionary size.

        rezimi = parse_data(tree, "/VozniRediXML/Avtobus/Rezimi")
        prevozniki = parse_data(tree, "/VozniRediXML/Avtobus/Prevozniki")
        postajalisca = parse_data(tree, "/VozniRediXML/Avtobus/Postajalisca")
        relacije = parse_data(tree, "/VozniRediXML/Avtobus/Relacije")
        vozni_redi = parse_data(tree, "/VozniRediXML/Avtobus/VozniRedi")
        voznje = parse_data(tree, "/VozniRediXML/Avtobus/Voznje")
        opisi_vozenj = parse_data(tree, "/VozniRediXML/Avtobus/OpisiVozenj")

        # Database import
        for rezim in rezimi:
            r = Regime()
            r.author = rezim["Avtor"]
            r.id = rezim["OznakaRezima"]
            r.description = rezim["Opis"]
            r.save()

        for prevoznik in prevozniki:
            p = Transporter()
            p.id = prevoznik["Prevoznik"]
            p.name = prevoznik["Ime"]
            p.person = prevoznik["Oseba"]
            p.email = prevoznik["Email"]
            p.address = prevoznik["Naslov"]
            p.save()

        models = []
        for postajalisce in postajalisca:
            p = Station()
            p.number = to_int_or_none(postajalisce["Stevilka"])
            p.name = postajalisce["Postajalisce"]
            p.dtk25 = postajalisce["DTK25"]
            p.meaning = postajalisce["Pomen"]

            # XXX Workaround for SQLite, replace when switching engines
            if len(models) > 100:
                Station.objects.bulk_create(models)
                models = []
            models.append(p)
        Station.objects.bulk_create(models)

        models = []
        for relacija in relacije:
            r = Route()
            r.start = relacija["ZacetekRelacije"]
            r.start_author = relacija["AvtorZacetekRelacije"]
            r.end = relacija["KonecRelacije"]
            r.end_author = relacija["AvtorKonecRelacije"]

            r.mk_km = to_int_or_none(relacija["MKkm"])
            r.mk_time = to_int_or_none(relacija["MKcas"])
            r.pr_km = to_int_or_none(relacija["PRkm"])
            r.pr_time = to_int_or_none(relacija["PRcas"])

            if len(models) > 100:
                Route.objects.bulk_create(models)
                models = []
            models.append(r)

        Route.objects.bulk_create(models)

        models = []
        for vozni_red in vozni_redi:
            s = Schedule()
            s.type = vozni_red["Vrsta"]
            s.version = vozni_red["Verzija"]
            s.label = vozni_red["Oznacba"]
            s.line_from = vozni_red["LinijaOd"]
            s.line_to = vozni_red["LinijaDo"]
            s.year = to_int_or_none(vozni_red["Leto"])
            s.way = vozni_red["Nacin"]
            s.valid_from = vozni_red["Od"]
            s.valid_to = vozni_red["Do"]

            if len(models) > 30:
                Schedule.objects.bulk_create(models)
                models = []
            models.append(s)
        Schedule.objects.bulk_create(models)

        models = []
        for voznja in voznje:
            v = Ride()
            v.label = voznja["OznakaVoznje"]
            v.direction = voznja["SmerVoznje"]
            v.time = voznja["Cas"]
            v.cooperant = voznja["Kooperant"]
            v.number = to_int_or_none(voznja["StevilkaVoznje"])

            if len(models) > 50:
                Ride.objects.bulk_create(models)
                models = []
            models.append(v)

        Ride.objects.bulk_create(models)



def to_int_or_none(val):
    if val is None:
        return None

    try:
        ret = int(val)
    except ValueError:
        return None

    return ret

def parse_data(tree, path):
    """
    Parses flat-structured XML data
    """
    xml_node = tree.xpath(path)
    data = []

    for top_el in xml_node:
        dict = {}
        for element in top_el:
            if dict.has_key(element.tag):
                data.append(dict)
                dict = {}
            dict[element.tag] = element.text

    return data