from django.db import models

# Create your models here.
class Regime(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    description = models.TextField()
    author = models.TextField()

    def __unicode__(self):
        return "[%s][%s] %s" % (self.id, self.author, self.description, )

class Transporter(models.Model):
    id = models.CharField(max_length=20, primary_key=True)
    name = models.TextField()
    address = models.TextField(null=True)
    person = models.TextField(null=True)
    email = models.TextField(null=True)

    def __unicode__(self):
        return "[%s] %s" % (self.id, self.name, )

class Station(models.Model):
    number = models.IntegerField(null=True)
    name = models.TextField()
    meaning = models.TextField(null=True)
    dtk25 = models.TextField(null=True)

    def __unicode__(self):
        return "[%s] %s" % (self.number, self.name)

class Route(models.Model):
    start = models.TextField()
    start_author = models.TextField(null=True)
    end = models.TextField()
    end_author = models.TextField(null=True)
    mk_km = models.IntegerField(null=True)
    mk_time = models.IntegerField(null=True)
    pr_km = models.IntegerField(null=True)
    pr_time = models.IntegerField(null=True)

    def __unicode__(self):
        return "%s - %s" % (self.start, self.end)

class Schedule(models.Model):
    type = models.CharField(max_length=20)
    version = models.CharField(max_length=50)
    label = models.CharField(max_length=50)
    line_from = models.TextField()
    line_to = models.TextField()
    year = models.IntegerField()
    way = models.CharField(max_length=50)
    valid_from = models.DateTimeField()
    valid_to = models.DateTimeField()

    def __unicode__(self):
        return "%s - %s" % (self.line_from, self.line_to)

class Ride(models.Model):
    label = models.CharField(max_length=20)
    direction = models.CharField(max_length=1)
    time = models.TimeField()
    cooperant = models.CharField(max_length=20)
    number = models.IntegerField()

class RideDescription(models.Model):
    station_number = models.IntegerField()
    arrival = models.TimeField()
    departure = models.TimeField()
    stop = models.CharField(max_length=5)
    chainage = models.CharField(max_length=3)